Beos' BabyBe clone developed with pygame

In order to play you need:

 * pygame (python-pygame)
 * espeak

The game is very simple: 
* Press a key and it will get rendered on a random position with a random size at screen. 
* Press a mouse button and a random geometric form will be drawed
* 'Esc' cleans the screen and puts a random background colour
* 'Intro' pronounces the last pressed keys. I.e. Press "h","e","l","p", "intro"-> The app invokes espeak with the "help" string as argument.

Note:
In ubuntu bionic it seems that pygame is only available for python2. If that is your case then modidy the first line of the app from "#!/usr/bin/python3" to "#!/usr/bin/python"
